# Entrega MetNum TP1

## Aclaracion test:

Para correr los test del cpp, se requiere mover la carpeta /tests (test dados por la catedra) dentro de la carpeta /src.

## Calcular rankings :

Para calcular un ranking, dentro del /scr/cpp se toma por línea de comandos tres parámetros:

- El primero de ellos
contiene el path al archivo de entrada con los partidos y resultados de la competencia;

- El segundo la salida con el ranking correspondiente

- El tercero indica el método a
considerar: 0 CMM, 1 WP, 2 RPI

Ejemplo:
$ ./tp1 partidos.csv ranking.out 0

El archivo de entrada contiene primero una línea con información sobre la cantidad
de participantes/equipos T , y la cantidad de partidos totales a considerar P. Luego,
siguen P líneas donde cada una de ellas representa un partido y contiene la siguiente información separada por espacios o tabulador: 

- identificador de fecha (int, se ignora),
- equipo i, 
- puntos equipo i, 
- equipo j, 
- puntos equipo j.

Los números de equipos deben estar entre 1 y n, siendo n el total.

En caso de que los equipos no estén numerados de 1 a n, se debe codificar el achivo previamente con ./codificarParticipantes

El programa toma por línea de comandos tres parámetros.

- El primero de ellos
contiene el path al archivo de entrada con los partidos y resultados de la competencia sin codificar;
- El segundo la salida con el ranking correspondiente que tendrá la misma estructura pero con los números de equipo codificados
- El tercero indica en cada línea que participante original tiene dicho código.

Ejemplo:
$ ./codificarParticipantes partidos-sin-traducir.csv partidos-codificados.in
codigo.txt

## Experimentos justicia CMM :

Los experimentos de justicia CMM se corren desde un jupyter notebook que se encuentra en la carpeta /src/analitycs/justicia_CMM

Dentro del notebook se grafican los resultados y se pueden cambiar los parametros de experimentacion.

Los archivos generados por el notebook se almacenan en la carpeta ./torneos dentro de ./justicia_CMM

## Competencias reales

Los archivos para los experimentos usados para experimentar con la NBA se encuentran en data/competencias reales/NBA/nba2016.csv y para el ATP data/competencias reales/ATP/ATPtrad.in

:D